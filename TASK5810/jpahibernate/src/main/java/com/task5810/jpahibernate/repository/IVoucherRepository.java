package com.task5810.jpahibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task5810.jpahibernate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
