package com.task5810.jpahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@ComponentScan({"com.edu", "com.abc"})
@SpringBootApplication //(scanBasePackages = {"com.edu", "com.abc"})
public class JpahibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpahibernateApplication.class, args);
	}

}
