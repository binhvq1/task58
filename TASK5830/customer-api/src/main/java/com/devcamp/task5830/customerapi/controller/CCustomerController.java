package com.devcamp.task5830.customerapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5830.customerapi.repository.ICustomerRepository;
import com.devcamp.task5830.customerapi.model.CCustomer;

@RestController
@CrossOrigin
public class CCustomerController {
    @Autowired
    ICustomerRepository iCustomerRepository;
    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllDrink() {
        try {
            List<CCustomer> cCustomers = new ArrayList<CCustomer>();
            iCustomerRepository.findAll().forEach(cCustomers::add);
            return new ResponseEntity<>(cCustomers, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
