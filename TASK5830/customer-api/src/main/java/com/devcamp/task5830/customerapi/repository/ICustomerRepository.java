package com.devcamp.task5830.customerapi.repository;

import org.aspectj.apache.bcel.util.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5830.customerapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    
}
