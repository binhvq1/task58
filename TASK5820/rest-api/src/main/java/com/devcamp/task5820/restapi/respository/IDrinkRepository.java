package com.devcamp.task5820.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5820.restapi.model.CDdrink;

public interface IDrinkRepository extends JpaRepository<CDdrink,Long> {
    
}
